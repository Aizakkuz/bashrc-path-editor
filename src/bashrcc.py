import gi
import os
import shutil
import subprocess

gi.require_version("Gtk", "3.0")
gi.require_version('Notify', '0.7')

from gi.repository import Gtk
from gi.repository import Notify
from gi.repository import GdkPixbuf
from gi.repository import GObject
from datetime import datetime

CURRENT_PATH_VARIABLE = os.getenv('PATH')
CURRENT_USER_NAME = os.getenv('USER')
ALL_PATHS = CURRENT_PATH_VARIABLE.split(":")
BACKUP_FOLDER = '/home/' + CURRENT_USER_NAME + '/Bashrc Backups'

def remove_spaces(STRING):
   return STRING.replace(" ", "").strip()

def _check_valid_directory(PATH):
    IsValidDir = os.path.isdir(PATH)
    return IsValidDir

def _check_for_matching_directory(PATH):
    NoMatchingDirectory = True
    for i in range(0, len(ALL_PATHS)):
        path = ALL_PATHS[i]
        if path == PATH:
            NoMatchingDirectory = False   

    return NoMatchingDirectory

def _is_latest_path_addition_not_same(PATH):
    LatestPathAddNotSame = True
    BASHRC = open('/home/' + CURRENT_USER_NAME + '/.bashrc', "r")
    LAST_LINE = BASHRC.readlines()[-1]
    LAST_PATH = LAST_LINE.replace("export PATH=", "").replace(":$PATH", "").replace('"', "")

    print("LAST PATH:" + remove_spaces(LAST_PATH), "PATH:" + remove_spaces(PATH), "CHECK IF SAME?", remove_spaces(PATH) == remove_spaces(LAST_PATH))
    if remove_spaces(PATH) == remove_spaces(LAST_PATH):
        LatestPathAddNotSame = False

    return LatestPathAddNotSame         

def _path_selected(BUTTON, PATH):
    BUTTON.show()
   
def selected_file(self, PATH_ENTRY):
    G_FILE = self.get_file() 
    PARENT_G_FILE = G_FILE.get_parent()

    PATH_ENTRY.set_text(PARENT_G_FILE.get_path())

def edit_bashrc(PATH):
    BASHRC = open('/home/' + CURRENT_USER_NAME + '/.bashrc', "r")
    BASHRC_READ = BASHRC.readlines()
    BASHRC_LC = len(BASHRC_READ)
    
    BASHRC_READ.append('export PATH="' + PATH + ':$PATH"\n') 
    BASHRC = open('/home/' + CURRENT_USER_NAME + '/.bashrc', "w")  
    BASHRC.writelines(BASHRC_READ)
    BASHRC.close()

def create_backups_folder():
    if not _check_valid_directory(BACKUP_FOLDER):
         os.makedirs(BACKUP_FOLDER)

def create_backup():
    EXACT_TIME = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    create_backups_folder()
    ORG = r'/home/' + CURRENT_USER_NAME + '/.bashrc'
    TAR = r'/home/' + CURRENT_USER_NAME + '/Bashrc Backups' + '/.bashrc_copy -' + EXACT_TIME

    shutil.copyfile(ORG, TAR)

def activate(self, PATH_ENTRY):

    CURRENT_TEXT = PATH_ENTRY.get_text() 
    _path_selected(builder.get_object("ApplyButton"), CURRENT_TEXT)
    if _check_valid_directory(CURRENT_TEXT) and _check_for_matching_directory(CURRENT_TEXT) and _is_latest_path_addition_not_same(CURRENT_TEXT):
        Notify.Notification.new("✅ SUCCESSFULLY ADDED BIN ✅ ").show()  
        Notify.Notification.new("Bin Added! ⬇️ \n - \n " + CURRENT_TEXT + "❗").show() 

        create_backup()
        edit_bashrc(CURRENT_TEXT)
    else:
        Notify.Notification.new("🚫 BIN INVALID OR ALREADY IN USE❗").show() 
        Notify.Notification.new("Bin Attempting To Add ⬇️ \n - \n " + CURRENT_TEXT + "❗").show() 
        # Bin Attempting To Add ⬇️ \n " + CURRENT_TEXT + " ❗




class Handler:
    def __init__(self):
        # self.CURRENT_PATH_VARIABLE = os.getenv('PATH')
        # self.ALL_PATHS = self.CURRENT_PATH_VARIABLE.split(":")
        self.ALL_OBJECTS = builder.get_objects()
        self.PATH_BIN_COMBOBOX = self.ALL_OBJECTS[21]
        self.FILE_CHOOSER_BUTTON = self.ALL_OBJECTS[18]
        self.PATH_ENTRY = builder.get_object("Path_Entry")
        self.APPLY_BUTTON = builder.get_object("ApplyButton")

        self.APPLY_BUTTON.connect("clicked", activate, self.PATH_ENTRY)
        self.FILE_CHOOSER_BUTTON.connect("file-set", selected_file, self.PATH_ENTRY)

        #FILE_CHOOSER.file_set

        # for x in range(len(ALL_PATHS)):
        #     item = ALL_PATHS[x]
        #     print(x, item)

        # for x in range(len(self.ALL_OBJECTS)):
        #     item = self.ALL_OBJECTS[x]
        #     print(x, item)

        Notify.Notification.new("BashRC Editor Successfully Started ▶️").show()

Notify.init("BashRC Editor ✏️")
builder = Gtk.Builder()
builder.add_from_file("Glade Design BashRCC2.glade")
builder.connect_signals(Handler())

window = builder.get_object("MainWindow")
window.show_all()
window.connect("destroy", Gtk.main_quit)

Gtk.main()



# viewport = builder.get_object("imageloader_viewport")

# loading_image = Gtk.Image()
# loading_image.set_from_animation(gif_anim)
# viewport.add(loading_image)
# loading_image.show_all()

# gif_path = "BASH RCC LOGO GIF.gif"
# gif_anim = GdkPixbuf.PixbufAnimation.new_from_file(gif_path)
